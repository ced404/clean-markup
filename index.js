#!/usr/bin/env node
// https://developer.atlassian.com/blog/2015/11/scripting-with-node/
var program = require("commander");
var fs = require("fs");
var path = require("path");
var sanitizeHtml = require("sanitize-html");
var Minify = require("html-minifier").minify;

const minifyContent = function (html) {
	let min = Minify(html, minifyOptions);
	return min;
};

const processFile = function (dirname, filename, html) {
	let filepath = path.join(dirname, filename);
	let outputpath = path.join(dirname, filename.replace(".html", "") + "-stripped.html");
	let result = sanitizeHtml(html, sanitizeHtmlOptions);

	if (options.minify) result = minifyContent(result);

	fs.writeFile(outputpath, result, function (err) {
		if (err) return console.log(err);
		console.log(`⊡ Processed ${filepath} to ${outputpath}`);
	});
};

const fileError = function (error) {
	console.error("error:", error);
};

const readFiles = function (dirname, onFileContent, onError) {
	var dirents = null;
	try {
		dirents = fs.readdirSync(dirname, {withFileTypes: true});
	} catch (err) {
		if (err.code === "ENOENT") {
			console.log("\nCannot process that directory!");
		} else {
			throw err;
		}
		return false;
	}

	// filter .html files
	let filesNames = dirents.filter((dirent) => {
		return dirent.isFile() && path.extname(dirent.name).toLowerCase() === ".html";
	});
	// return only filenames
	filesNames = filesNames.map((dirent) => dirent.name);

	if (!filesNames.length) return console.log("No .html files in directory");

	filesNames.forEach(function (filename) {
		let filepath = path.join(dirname, filename);
		fs.readFile(filepath, "utf-8", function (err, content) {
			if (err) {
				onError(err);
				return;
			}
			onFileContent(dirname, filename, content);
		});
	});
};

const scanDirectory = function (dir = "./") {
	readFiles(dir, processFile, fileError);
};

// Get program arguments
program
	.option("-d, --dir <value>", "Directory to scan for .html files", "./")
	.option(
		"-t, --tags <items>",
		"Comma separated list of allowed tags, 'all' or 'none')",
		"h1,h2,h3,h4,h5,h6,blockquote,p,a,ul,ol,nl,li,b,i,strong,em,strike,code,hr,br,div,table,thead,caption,tbody,tr,th,td,pre,iframe"
	)
	.option("-dt, --document-tags", "Keep document tags (doctype, html, head, body, title)", false)
	.option(
		"-a, --attributes <items>",
		"Comma separated list of allowed attributes, 'all' or 'none'",
		"href,name,alt,target,src"
	)
	.option("-m, --minify", "Minify with html-minifer", false)
	.version("1.3.0")
	.parse(process.argv);

// Setup AttributeRemover
var options = {};
options.dir = program.dir;

// allow all tags
if (program.tags === "all") options.tags = false;
// strip all tags
else if (program.tags === "none") options.tags = [];
// an array of allowed tags
else options.tags = program.tags.split(",").map((i) => i.trim());

// keep document tags?
if (options.tags !== false && program.documentTags === true)
	options.tags = [...options.tags, "html", "head", "body", "doctype", "title"];

// allow all attributes
if (program.attributes === "all") options.attributes = false;
// strip all attributes
else if (program.attributes === "none") options.attributes = [];
// an array of allowed attributes
else options.attributes = program.attributes.split(",").map((i) => i.trim());

// Minify HTML?
options.minify = program.minify;

// https://www.npmjs.com/package/sanitize-html
// Defaults
var sanitizeHtmlOptions = {
	allowedTags: options.tags,
	allowedAttributes: {
		"*": options.attributes,
	},
	// selfClosing: ["img", "br", "hr", "area", "base", "basefont", "input", "link", "meta"],
	parser: {
		lowerCaseTags: true,
	},
};

var minifyOptions = {
	removeAttributeQuotes: false,
	collapseWhitespace: true,
	removeComments: true,
	removeOptionalTags: false,
	removeRedundantAttributes: true,
	removeScriptTypeAttributes: true,
	removeTagWhitespace: true,
	useShortDoctype: true,
	minifyCSS: true,
	minifyJS: true,
};

console.log("\033[1m" + `Processing .html files in ${options.dir}` + "\033[0m");
console.log(options);

// Scan directory and process files
scanDirectory(program.dir);
