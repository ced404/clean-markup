# clean-markup

Simple NodeJS command line tool based on [sanitize-html](https://www.npmjs.com/package/sanitize-html) to remove tags and attributes form HTML files.

Optional minification with [html-minifier](https://www.npmjs.com/package/html-minifier).

Maybe you should use [clean-html](https://www.npmjs.com/package/clean-html)!


```shell
Usage: clean-markup [options]

Options:
  -d, --dir <value>         Directory to scan for .html files (default: "./")
  -t, --tags <items>        Comma separated list of allowed tags, 'all' or 'none') (default:
                            "h1,h2,h3,h4,h5,h6,blockquote,p,a,ul,ol,nl,li,b,i,strong,em,strike,code,hr,br,div,table,thead,caption,tbody,tr,th,td,pre,iframe")
  -dt, --document-tags      Keep document tags (doctype, html, head, body, title) (default: false)
  -a, --attributes <items>  Comma separated list of allowed attributes, 'all' or 'none' (default: "href,name,alt,target,src")
  -m, --minify              Minify with html-minifer (default: false)
  -V, --version             output the version number
  -h, --help                display help for command
```

**Example 1:** `clean-markup -d ./test-files -t a,p -a id,name -m`

- Will process all `.html` files from the `test-files` directory,
- keeping only `<p>` and `<a>` tags,
- removing attributes but `id` and `name`,
- minifying the HTML output.

**Example 2:** `clean-markup -d ./test-files -t all -a none -m`

- Will process all `.html` files from the `test-files` directory,
- keeping all tags,
- removing all attributes,
- minifying the HTML output.

**Example 2:** `clean-markup -d ./test-files -dt -a none -m`

- Will process all `.html` files from the `test-files` directory,
- keeping default tags and document tags,
- removing all attributes,
- minifying the HTML output.


## minify

Use html-minifier to compress HTML with these options:

```JSON
{
	"removeAttributeQuotes": false,
	"collapseWhitespace": true,
	"removeComments": true,
	"removeOptionalTags": false,
	"removeRedundantAttributes": true,
	"removeScriptTypeAttributes": true,
	"removeTagWhitespace": true,
	"useShortDoctype": true,
	"minifyCSS": true,
	"minifyJS": true
}
```
